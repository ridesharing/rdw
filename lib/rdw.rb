require 'rdw/version'
require 'rdw/parser'

module RDW

  class CarInfo

    def initialize(license_plate)
      @parser = Parser.new(license_plate)
    end

    def number_of_cylinders
      @parser.parse('Aantalcilinders').to_i
    end

    def number_of_seats
      @parser.parse('Aantalzitplaatsen').to_i
    end

    def license_plate
      @parser.parse('Kenteken')
    end

    def bpm
      @parser.parse('BPM').to_i
    end
    # Changed method name to lower case to follow coding conventions.
    alias_method :BPM, :bpm

    def fuel_efficiency_main_road
      @parser.parse('Brandstofverbruikbuitenweg').to_f
    end

    def fuel_efficiency_city
      @parser.parse('Brandstofverbruikstad').to_f
    end

    def fuel_efficiency_combined
      @parser.parse('Brandstofverbruikgecombineerd').to_f
    end

    def cylinder_capacity
      @parser.parse('Cilinderinhoud').to_f
    end

    def co2_combined
      @parser.parse('CO2uitstootgecombineerd').to_f
    end

    def first_color
      @parser.parse_color('Eerstekleur')
    end
    alias_method :color, :first_color

    def second_color
      @parser.parse_color('Tweedekleur')
    end

    def fuel_type
      @parser.parse_fuel('Hoofdbrandstof')
    end

    def brand
      @parser.parse('Merk')
    end

    def trade_name
      @parser.parse('Handelsbenaming')
    end

    def energy_label
      @parser.parse('Zuinigheidslabel')
    end

    def stock_price
      @parser.parse('Catalogusprijs')
    end

    def engine_power
      @parser.parse('Vermogen')
    end

    def inspect
      "<RDW::CarInfo license_plate:'#{license_plate}' brand:'#{brand}' fuel_type:'#{fuel_type}'>"
    end

    def exists
      !license_plate.nil?
    end

    def raw_data_field(attribute_name)
      @parser.parse(attribute_name)
    end

    # Map all properties to a hash. Can be useful for storing data in a database easier.
    def to_hash
      {
          license_plate: license_plate,
          number_of_seats: number_of_seats,
          # number_of_cylinders: number_of_cylinders,
          # bpm: bpm,
          fuel_efficiency_main_road: fuel_efficiency_main_road,
          fuel_efficiency_city: fuel_efficiency_city,
          fuel_efficiency_combined: fuel_efficiency_combined,
          # cylinder_capacity: cylinder_capacity,
          # co2_combined: co2_combined,
          first_color: first_color,
          second_color: second_color,
          fuel_type: fuel_type,
          brand: brand,
          trade_name: trade_name,
          # energy_label: energy_label,
          # stock_price: stock_price,
          # engine_power: engine_power,

      }
    end




  end
end
