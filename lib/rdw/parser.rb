require 'net/http'
require 'nokogiri'
require 'i18n'

module RDW

  class Parser

    def initialize(license_plate)
      @license_plate = license_plate
      @colors = {
          'oranje' => '#FFA500',
          'roze' => '#FF69B4',
          'rood' => '#FF0000',
          'wit' => '#FFFFFF',
          'blauw' => '#1E90FF',
          'groen' => '#32CD32',
          'geel' => '#FFD700',
          'grijs' => '#A9A9A9',
          'bruin' => '#8B4513',
          'creme' => '#FFE4B5',
          'paars' => '#8A2BE2',
          'zwart' => '#000000',
          'diversen' => 'mixed',
      }
      @fuel_types = {
          'Benzine' => 'gasoline',
          'Liquified Natural Gas' => 'lng',
          'LNG' => 'lng',
          'Diesel' => 'diesel',
          'Elektriciteit' => 'electric',
          'Liquified Petrol Gas' => 'lpg',
          'LPG' => 'lpg',
          'Compressed Natural Gas' => 'cng',
          'CNG' => 'cng',
          'Waterstof' => 'hydrogen',
          'Alcohol' => 'alcohol'
      }
      perform_request!

    end

    def parse(attribute_name)
      begin
        @xml.xpath("//d:#{attribute_name.to_s}").text
      rescue
        nil
      end
    end

    def parse_color(attribute_name)
      text = parse(attribute_name)
      unless text.nil?
        text.downcase!
      end
      if @colors.keys.include? text
        @colors[text]
      else
        nil
      end
    end

    def parse_boolean(attribute_name)
      text = parse(attribute_name)
      if text == 'Ja'
        true
      elsif text == 'Nee'
        false
      else
        nil
      end
    end

    def parse_fuel(attribute_name)
      text = parse(attribute_name)
      unless text.nil?
        text.downcase!
      end
      @fuel_types.each do |key, value|
        if !text.nil? && text.include?(key.downcase)
          return value
        end
      end
      nil
    end

    def perform_request!
      response = Net::HTTP.get_response(URI("https://api.datamarket.azure.com/Data.ashx/opendata.rdw/VRTG.Open.Data/v1/KENT_VRTG_O_DAT('#{@license_plate}')"))
      @xml = Nokogiri::XML(response.body)
    end
  end
end
